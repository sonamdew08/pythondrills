# Save the first n natural numbers and their squares into a file in the csv format.

myfile = open("number_and_square.csv", 'w')


def numbers_and_squares(n, file_path):
    for i in range(1, n+1):
        print(i, i*i)
        
        myfile.write(str(i) + ',' + str(i*i) + " ")

numbers_and_squares(6, "number_and_square.csv")
myfile.close()
   
