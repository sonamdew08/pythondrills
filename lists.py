x = [3, 1, 2, 4, 1, 2]

# Find the sum of all the elements in x
total = 0
for item in x:
    total += item
print(total)

# Find the length of x
print(len(x))

# Print the last three items of x
print(x[3:])

# Print the first three items of x
print(x[:3])

# Sort x
x.sort()

# Add another item, 10 to x
x. append(10)
print(x)

# Add another item 11 to x
x. append(11)
print(x)

# Remove the last item from x
x.pop()
print(x)

# How many times does 1 occur in x?
count = 0
for item in x:
    if item is 1:
        count += 1

print(count)

# Check whether the element 10 is present in x
check = "No"
for item in x:
    if item is 10:
        check = "yes"
        break

print(check)

y = [5, 1, 2, 3]

# Add all the elements of y to x
x += y
print(x)

# Create a copy of x
copy_x = x
print(copy_x)

# Can a list contain elements of different data-types?
#yes, a list can contain element of different types
item_list = [1, "Two", True, 3.5]
print(item_list)

# Which data structure does Python use to implement a list?
#LIFO