# Write a function that accepts an integer n, n > 0, and returns a list of all n-digit prime numbers

def n_digit_prime(n):
    digit_num = 1
    while n > 1:
        digit_num *= 10
        n -= 1
    # print(digit_num)

    primes = []
    
    for num in range(digit_num, digit_num * 10):
        if(is_prime(num)):
            primes.append(num)
            
    return primes

# write a helper function, is_prime that returns whether a number is prime or not, and use it in your n_digit_primes function

def is_prime(num):
    for div in range(2, num + 1):
        if num == div:
            return True
        if num % div is 0:
            return False

print(n_digit_prime(3))
