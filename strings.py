x = "one two three four"

# Print the last 3 characters of x
print(x[-3:])

# Print the first 10 characters of x
print(x[:10])

# Print characetrs 4 through 10 of x
print(x[4:10])

# Find the length of x
print(len(x))

# Split x into its words
print(x.split(" "))

# Capitalize the first character of x
print(x.capitalize())

# Convert x into uppercase
print(x.upper())

y = x
x = 'one two three'
# After executing the above code, what is the value of y?
print(y)