# Write a function to find the number of occurrences of each word in a string

def word_count(str=""):
    num_of_occ = {}
    if len(str) is 0:
        return num_of_occ
    else:
        word = str.split(" ")
        
        for i in word:
            
            length_word = len(i)
            if i[length_word-1] is "." or i[length_word-1] is ",":
                i = i[:-1]

            
            if num_of_occ.get(i) is None:
                num_of_occ[i] = 1
            else:
                num_of_occ[i] += 1
        return num_of_occ

# Define a function that prints all the items (keys, values) in a dictionary

def print_dict_keys_and_values(d):
    print("{")
    for key in d:
        print(str(key) + ":" + str(d[key]))
    print("}")

# Define a function that returns a list of all the items in a dictionary sorted by the dictionary's keys.
def list_of_items_sorted_by_keys(d):
    d_list = []
    for i in sorted(d):
        d_list.append((i, d[i]))
   
    return d_list
    


num_of_words = word_count("Python is an interpreted, high-level, general-purpose programming language. Python interpreters are available for many operating systems. Python is a multi-paradigm programming language. Object-oriented programming and structured programming are fully supported.")
print_dict_keys_and_values(num_of_words)
print(list_of_items_sorted_by_keys(num_of_words))
# print(num_of_words)
print_dict_keys_and_values(word_count("hello world hello hello"))
print(word_count()) #if string is empty


    


