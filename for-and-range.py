x = [1, 2, 3, 4]

# Print all the elements in x
for i in x:
    print(i)

# Print all the elements and their indexes using the enumerate function
for i in enumerate(x):
    print(i)

# Print all the integers from 10 to 0 in descending order using range
for i in range(10, -1, -1):
    print(i)

# Print all the integers from 10 to 0 in descending order using while
num = 10
while num > -1:
    print(num)
    num -= 1
