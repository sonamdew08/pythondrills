x = 1
y = 10
if x > y:
    x = y
else:
    y = x + 1

#What is y after the above code is executed?
# output is 2
print(y)