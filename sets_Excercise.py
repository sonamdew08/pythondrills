# Define a function that does the equivalent of uniq <file_path> | sort, i.e., the function should

# Accept a file path as the argument
# Read the contents of the file into a string
# Split the string into lines
# Remove duplicate lines
# Sort the lines
# Return the sorted lines
myfile = open("sample.txt", "r")

# Define a function
def uniq_set(myfile):                           # Accept a file path as the argument
    sorted_line = []
    file_string = myfile.read()                 # Read the contents of the file into a string
    file_split = file_string.split(" ")         # Split the string into lines
    file_remove = set(file_split)               # Remove duplicate lines
    for i in sorted(file_remove):               # Sort the lines
        sorted_line.append(i)
    
    return " ".join(sorted_line)                # Return the sorted lines

print(uniq_set(myfile))