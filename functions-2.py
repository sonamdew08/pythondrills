
# To the n_digit_primes function, set the default value of argument n to 2
def n_digit_prime(n=2):
    digit_num = 1
    while n > 1:
        digit_num *= 10
        n -= 1
    # print(digit_num)

    primes = []
    
    for num in range(digit_num, digit_num * 10):
        if(is_prime(num)):
            primes.append(num)
            
    return primes

def is_prime(num):
    for div in range(2, num + 1):
        if num == div:
            return True
        if num % div is 0:
            return False

print(n_digit_prime())

# Call the n_digit_primes with a keyword argument, n_digit_primes(n=1)
print(n_digit_prime(n=1))

#Define a function, args_sum that accepts an arbitrary number of integers as input and computes their sum.

def args_sum(*args, **kwargs):
    sum = 0
    
    for i in args:
        sum += i
    for i in kwargs:
        if kwargs[i] is True:
            
            return abs(sum)
        else:
            return sum
        


# Modify the args_sum function so that it accepts an optional, boolean, keyword argument named absolute. 
# If absolute is True, then args_sum must return the absolute value of the sum of *args.
# If absolute is not specified, then return the sum without performing any conversion.
print(args_sum(-1, -2, -3, -4, absolute=True))
print(args_sum(-1, -2, -3, -4, absolute=False))
print(args_sum(34, 56))
